# Vog Backend Code Challenge #

### What is this repository for? ###

* This code challenge is observering my coding skills, familiarity with C#, ASP.Net core framework,
Entity Framework, Unit Testing, utilizing best practices, clean coding, reusability, version control,
teamwork and problem solving.
* Version 1.0.0.0

### How to setup? ###

* Clone this repository.
* Build the solution using Visual Studio, or on the command line with dotnet build.
* Run the project with donet run. The API will start up on http://localhost:5000, or https://localhost:5001.
* Use an HTTP client like Postman.

### List of APIs ###

* Get All Employees: GET https://localhost:5001/api/employees
* Get All Employees By Department ID: GET https://localhost:5001/api/employees/department/{departmentId}
