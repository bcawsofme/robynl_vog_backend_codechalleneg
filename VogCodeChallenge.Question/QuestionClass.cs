namespace VogCodeChallenge.Question
{
    public static class QuestionClass
    {
        static List<string> NamesList = new List<string>()
        {
            "Jimmy",
            "jeffrey"
        };

        public static void TestQuestion()
        {
            // Initial Implementation
            /*using (var enumerator = NamesList.GetEnumerator())
            {
                while (enumerator.MoveNext())
                {
                    Console.WriteLine(enumerator.Current);
                }
            }*/

            // Easier Syntax Solution (Linq)
            NamesList.ForEach(name => Console.WriteLine(name));
        }
    }
}