using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using VogCodeChallenge.API.Models;
using VogCodeChallenge.API.Service;

namespace VogCodeChallenge.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class EmployeesController : ControllerBase
    {
        private readonly EmployeeService employeeService;

        public EmployeesController(EmployeeService employeeService)
        {
            this.employeeService = employeeService;
        }
        
        [HttpGet]
        public ActionResult<IEnumerable<Employee>> GetAllEmployees()
        {
            return new ActionResult<IEnumerable<Employee>>(employeeService.GetAll());
        }

        [HttpGet("department/{departmentId}")]
        public ActionResult<IEnumerable<Employee>> GetEmployeesByDepartment(Guid departmentId)
        {
            return new ActionResult<IEnumerable<Employee>>(employeeService.GetEmployeesByDepartmentId(departmentId));
        }
    }
}
