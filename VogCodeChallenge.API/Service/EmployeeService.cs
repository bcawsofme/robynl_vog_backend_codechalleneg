using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using VogCodeChallenge.API.Models;

namespace VogCodeChallenge.API.Service
{
    public class EmployeeService : IEmployeeService
    {
        private readonly IEmployeeRepository repository;

        public EmployeeService(IEmployeeRepository repository)
        {
            this.repository = repository;
            SeedData.PopulateData(repository);
        }     
        public IList<Employee> ListAll()
        {
            return repository.ListAll();
        }

        public IEnumerable<Employee> GetAll()
        {
            return repository.GetAll();
        }

        public IEnumerable<Employee> GetEmployeesByDepartmentId(Guid departmentId)
        {
            return repository.GetEmployeesByDepartmentId(departmentId);
        }
    }
}