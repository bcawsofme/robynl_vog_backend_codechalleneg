using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using VogCodeChallenge.API.Models;

namespace VogCodeChallenge.API.Service
{
    public interface IEmployeeService
    {
        IList<Employee> ListAll();
        
        IEnumerable<Employee> GetAll();

        IEnumerable<Employee> GetEmployeesByDepartmentId(Guid departmentId);  
    }
}