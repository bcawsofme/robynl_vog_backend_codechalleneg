using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using VogCodeChallenge.API.Models;

namespace VogCodeChallenge.API.Service
{
    public interface IEmployeeRepository
    {
        void SeedEmployees(List<Employee> employees);
        
        void SeedDepartments(List<Department> departments);

        IEnumerable<Employee> GetAll();
        
        IList<Employee> ListAll();

        IEnumerable<Employee> GetEmployeesByDepartmentId(Guid departmentId);
    }
}