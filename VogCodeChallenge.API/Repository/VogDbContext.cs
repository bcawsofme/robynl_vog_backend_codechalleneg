using Microsoft.EntityFrameworkCore;
using VogCodeChallenge.API.Models;

namespace VogCodeChallenge.API.Repository
{
    public class VogDbContext : DbContext
    {
        public DbSet<Employee> Employees { get; set; }
        public DbSet<Department> Departments { get; set; }

        public VogDbContext(DbContextOptions<VogDbContext> options) : base(options)
        {

        }

        protected override void OnModelCreating(ModelBuilder builder)
        {            
            builder.Entity<Department>()
                .HasOne(x => x.Employees)
                .WithMany()
                .HasForeignKey(x => x.Id);
        }
    }
}