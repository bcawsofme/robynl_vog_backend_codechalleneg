using System;
using System.Collections.Generic;
using System.Linq;
using VogCodeChallenge.API.Models;
using VogCodeChallenge.API.Service;

namespace VogCodeChallenge.API.Repository
{
    public class EmployeeDBRepository : IEmployeeRepository
    {
         private readonly VogDbContext dbContext;

        public EmployeeDBRepository(VogDbContext dbContext)
        {
            this.dbContext = dbContext;
        }

        public void SeedEmployees(List<Employee> employees) 
        { 
            if (dbContext.Employees.Count() < 1) {
                dbContext.AddRange(employees);
                dbContext.SaveChanges();
            }
        }

        public void SeedDepartments(List<Department> departments) 
        { 
            if (dbContext.Departments.Count() < 1) {
                dbContext.AddRange(departments);
                dbContext.SaveChanges();
            }
        }

        public IEnumerable<Employee> GetAll()
        {
            return dbContext.Employees;
        }

        public IList<Employee> ListAll()
        {
            return dbContext.Employees.ToList();
        }

        public IEnumerable<Employee> GetEmployeesByDepartmentId(Guid departmentId) 
        {
            Department department = dbContext.Departments.ToList().Find(x => x.Id.Equals(departmentId));
            return department.Employees;
        }
    }
}