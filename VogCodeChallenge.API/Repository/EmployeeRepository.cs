using System;
using System.Collections.Generic;
using VogCodeChallenge.API.Models;
using VogCodeChallenge.API.Service;

namespace VogCodeChallenge.API.Repository
{
    public class EmployeeRepository : IEmployeeRepository
    {
        private List<Department> departments;
        private List<Employee> employees;

        public EmployeeRepository()
        {
        }

        public void SeedEmployees(List<Employee> employees) 
        { 
            this.employees = employees; 
        }

        public void SeedDepartments(List<Department> departments) 
        { 
            this.departments = departments; 
        }

        public IEnumerable<Employee> GetAll()
        {
            return employees;
        }

        public IList<Employee> ListAll()
        {
            return employees;
        }

        public IEnumerable<Employee> GetEmployeesByDepartmentId(Guid departmentId) 
        {
            Department department = departments.Find(x => x.Id.Equals(departmentId));
            return department.Employees;
        }
    }
}