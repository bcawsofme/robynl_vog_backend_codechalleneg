using System;
using System.Collections.Generic;
using VogCodeChallenge.API.Models;

namespace VogCodeChallenge.API.Service
{
    public class SeedData
    {
        public static void PopulateData(IEmployeeRepository repository)
        {
            // Create Employees
            List<Employee> developers = new List<Employee>()
            {
                new Employee() {
                    Id = Guid.NewGuid(),
                    FirstName = "Robyn",
                    LastName = "Lee",
                    Position = "Developer",
                    StreetAddress = "123 Main Street",
                    City = "Calgary",
                    Province = "Alberta",
                    PostalCode = "T1Y5H3",
                    Country = "Canada",
                    PhoneNumber = "403-555-1234",
                    IsManager = false
                },
                new Employee() {
                    Id = Guid.NewGuid(),
                    FirstName = "Jane",
                    LastName = "Doe",
                    Position = "Developer",
                    StreetAddress = "123 Main Street",
                    City = "Calgary",
                    Province = "Alberta",
                    PostalCode = "T1Y5H3",
                    Country = "Canada",
                    PhoneNumber = "403-555-1234",
                    IsManager = false
                },
                new Employee() {
                    Id = Guid.NewGuid(),
                    FirstName = "Sam",
                    LastName = "Pierce",
                    Position = "Developer",
                    StreetAddress = "123 Main Street",
                    City = "Calgary",
                    Province = "Alberta",
                    PostalCode = "T1Y5H3",
                    Country = "Canada",
                    PhoneNumber = "403-555-1234",
                    IsManager = true
                }
            };

            List<Employee> qA = new List<Employee>()
            {
                new Employee() {
                    Id = Guid.NewGuid(),
                    FirstName = "John",
                    LastName = "Lee",
                    Position = "Developer",
                    StreetAddress = "123 Main Street",
                    City = "Calgary",
                    Province = "Alberta",
                    PostalCode = "T1Y5H3",
                    Country = "Canada",
                    PhoneNumber = "403-555-1234",
                    IsManager = false
                },
                new Employee() {
                    Id = Guid.NewGuid(),
                    FirstName = "David",
                    LastName = "Smith",
                    Position = "Developer",
                    StreetAddress = "123 Main Street",
                    City = "Calgary",
                    Province = "Alberta",
                    PostalCode = "T1Y5H3",
                    Country = "Canada",
                    PhoneNumber = "403-555-1234",
                    IsManager = false
                },
                new Employee() {
                    Id = Guid.NewGuid(),
                    FirstName = "Sam",
                    LastName = "Nixon",
                    Position = "Developer",
                    StreetAddress = "123 Main Street",
                    City = "Calgary",
                    Province = "Alberta",
                    PostalCode = "T1Y5H3",
                    Country = "Canada",
                    PhoneNumber = "403-555-1234",
                    IsManager = true
                }
            };

            // Create Departments
            List<Department> departments = new List<Department>() {
                new  Department() {
                    Id = new System.Guid("9d3e65db-dec8-4964-98fb-8984faf84c45"),
                    Name = "Development",
                    Employees = developers 
                },
                new  Department() {
                    Id = new System.Guid("dfb9deca-65f3-4795-a5e6-beb2324bf3dc"),
                    Name = "QualityAssurance",
                    Employees = qA
                }
            };

            // Join Developers & QA
            developers.AddRange(qA);

            // Seed Data
            repository.SeedEmployees(developers);
            repository.SeedDepartments(departments);
        }
    }
}